namespace Factory.Cards;

public static class CreditCardFactory
{

    /**
     * Manage the type as enum instead of string
     */
    public enum CardType
    {
        MoneyBack,
        Titanium,
        Platinum
    }

    public static ICreditCard? GetCreditCard(CardType cardType)
    {
        ICreditCard? cardDetails = null;

        if (cardType == CardType.MoneyBack)
        {
            cardDetails = new MoneyBack();
        }
        else if (cardType == CardType.Titanium)
        {
            cardDetails = new Titanium();
        }
        else if (cardType == CardType.Platinum)
        {
            cardDetails = new Platinum();
        }

        return cardDetails;
    }
}
