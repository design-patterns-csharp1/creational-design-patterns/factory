﻿using Factory.Cards;
using static Factory.Cards.CreditCardFactory;

string errorMessage = "I need one parameter. (yes|no)";

if (args.Length != 1)
{
    Console.WriteLine(errorMessage);
    return;
}

string type = args.FirstOrDefault()!.ToString();

if (type != "yes" && type != "no")
{
    Console.WriteLine(errorMessage);
    return;
}

if (type == "yes")
    ThisIsFactory();
else
    ThisIsNotFactory();


void ThisIsNotFactory()
{
    //Generally we will get the Card Type from UI.
    //Here we are hardcoded the card type
    string cardType = "MoneyBack";
    ICreditCard? cardDetails = null;
    //Based of the CreditCard Type we are creating the
    //appropriate type instance using if else condition
    if (cardType == "MoneyBack")
    {
        cardDetails = new MoneyBack();
    }
    else if (cardType == "Titanium")
    {
        cardDetails = new Titanium();
    }
    else if (cardType == "Platinum")
    {
        cardDetails = new Platinum();
    }
    if (cardDetails != null)
    {
        Console.WriteLine("CardType : " + cardDetails.GetCardType());
        Console.WriteLine("CreditLimit : " + cardDetails.GetCreditLimit());
        Console.WriteLine("AnnualCharge :" + cardDetails.GetAnnualCharge());
    }
    else
    {
        Console.WriteLine("Invalid Card Type");
    }
}

void ThisIsFactory()
{
    ICreditCard? cardDetails = CreditCardFactory.GetCreditCard(CardType.Platinum);

    if (cardDetails != null)
    {
        Console.WriteLine("CardType : " + cardDetails.GetCardType());
        Console.WriteLine("CreditLimit : " + cardDetails.GetCreditLimit());
        Console.WriteLine("AnnualCharge :" + cardDetails.GetAnnualCharge());
    }
    else
    {
        Console.WriteLine("Invalid Card Type");
    }

    Console.WriteLine("Hola");
}
